#include <conio.h>
#include <iostream>
#include <string>
#include <thread>
#include "hotplug_monitor.h"

using namespace std;

#define USB_ID_VENDOR_VALVE 0x28de
#define USB_ID_PRODUCT_WIRED 0x1102
#define USB_ID_PRODUCT_WIRELESS 0x1142

int hotplug_callback(libusb_context* usb_ctx, libusb_device* usb_dev, libusb_hotplug_event usb_event, void *user_data) {
	libusb_device_descriptor desc;
	libusb_get_device_descriptor(usb_dev, &desc);
	string s = usb_event == libusb_hotplug_event::LIBUSB_HOTPLUG_EVENT_DEVICE_ARRIVED ? "arrived" : "left";
	cout << endl << "device with VID:" << hex << desc.idVendor << " PID:" << hex << desc.idProduct << " just " << s << endl;
	return 1;
}

int main() {
	libusb_context* usb_ctx = NULL;
	if (libusb_init(&usb_ctx) != LIBUSB_SUCCESS) {
		return -1;
	}
	hm_start(usb_ctx); 
	//hm_register_callback((libusb_hotplug_event)(LIBUSB_HOTPLUG_EVENT_DEVICE_ARRIVED | LIBUSB_HOTPLUG_EVENT_DEVICE_LEFT), USB_ID_VENDOR_VALVE,-1, -1, (libusb_hotplug_callback_fn)hotplug_callback, nullptr);
	hm_register_callback((libusb_hotplug_event)(LIBUSB_HOTPLUG_EVENT_DEVICE_ARRIVED | LIBUSB_HOTPLUG_EVENT_DEVICE_LEFT), -1, -1, -1,(libusb_hotplug_callback_fn) hotplug_callback, nullptr);

	_getch();

	hm_remove_callback((libusb_hotplug_callback_fn)hotplug_callback);
	hm_stop();

	return 0;
}