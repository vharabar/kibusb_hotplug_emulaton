#pragma once
#ifndef HOTPLUG_MONITOR_H
#define HOTPLUG_MONITOR_H
#include <libusb-1.0\libusb.h>

#define MAX_CALLBACKS 64
#define MAX_DEVICES 128

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus


	int hm_start(libusb_context*);
	int hm_register_callback(libusb_hotplug_event events, int vendor_id, int product_id, int dev_class, libusb_hotplug_callback_fn cb, void* userData);
	int hm_remove_callback(libusb_hotplug_callback_fn);
	int hm_stop();

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // !HOTPLUG_MONITOR_H