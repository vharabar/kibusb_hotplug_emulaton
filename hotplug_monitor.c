#include <thr\threads.h>
#include "hotplug_monitor.h"
#include <libusb-1.0\libusb.h>

#if _DEBUG
#include <stdio.h>
#define HARD_EXIT
#endif

#ifndef HARD_EXIT
#define stop_thread(i) return i
#else
#define stop_thread(i) exit(i);
#endif // HARD_EXIT


typedef struct {
	libusb_hotplug_callback_fn fn;
	void* userData;
	int events;
	int vendor_id;
	int product_id;
	int dev_class;
} hm_callback_function;

typedef struct {
	libusb_context* ctx;
	int stop;
}hm_data;

typedef hm_data *hm_data_t;

struct {
	hm_callback_function callbacks[MAX_CALLBACKS];
	size_t cb_count;
	hm_data_t hm_data;
	thrd_t monitor;
	short started;
}hm_state = { {0}, 0,0,0 };

int find_device(libusb_device* dev, libusb_device** list, size_t count) {
	size_t i;
	int res = 0;
	for (i = 0; i < count; i++)
		if (list[i] == dev) {
			res = 1;
			break;
		}
	return res;
}

int delete_device(libusb_device* dev, libusb_device** list, size_t* count) {
	libusb_unref_device(dev);
	size_t i;
	int res = 0;
	for (i = 0; i < *count; i++)
		if (list[i] == dev) {
			list[i] = list[(*count) - 1];
			list[*count] = 0;
			(*count)--;
			res = 1;
			break;
		}
	return res;
}

int add_device(libusb_device* dev, libusb_device** list, size_t* count) {
	libusb_ref_device(dev);
	if (*count >= MAX_DEVICES)
		return 0;
	list[*count] = dev;
	(*count)++;
	return 1;
}

thrd_start_t hm_monitor(void* data) {
	int r; //for return values
	size_t i; //user for for loops
	ssize_t cnt; //holding number of devices in list
	hm_data_t d = (hm_data_t)data;
	libusb_device *old_devs[MAX_DEVICES] = { 0 };
	size_t old_count = 0;
	libusb_device **devs; //pointer to pointer of device, used to retrieve a list of devices
	
	//while hm_stop is not called
	while (!d->stop) {
		//getting connected device list
		cnt = libusb_get_device_list(d->ctx, &devs);
		if (cnt < 0) {
			stop_thread(-1);//end monitor prematurely because error
		}
		
		//loop devs to find new devices
		for (i = 0; i < cnt; i++) {
			struct libusb_device_descriptor desc;
			int r = libusb_get_device_descriptor(devs[i], &desc);
			if (r < 0) //if device descriptor is unaccessable
				continue; //ignore it
			if (desc.idVendor == 0 || desc.idProduct == 0) //ghost device
				continue; //ignore it;
			if (!find_device(devs[i], old_devs, old_count)) { //if the device was just connected
				//add device to the old_devs
				r = add_device(devs[i], old_devs, &old_count);
				if (!r)  //if we could not add a device
					stop_thread(-6);
				//Call on_arrived callbacks
				int c;
				for (c = 0; c < hm_state.cb_count; c++) {
					if (hm_state.callbacks[c].fn && (hm_state.callbacks[c].dev_class == (int)desc.bDeviceClass || hm_state.callbacks[c].dev_class == -1)
						&& (hm_state.callbacks[c].vendor_id == (int)desc.idVendor || hm_state.callbacks[c].vendor_id == -1)
						&& (hm_state.callbacks[c].product_id == (int)desc.idProduct || hm_state.callbacks[c].product_id == -1))
						if (hm_state.callbacks[c].events | LIBUSB_HOTPLUG_EVENT_DEVICE_ARRIVED)
							hm_state.callbacks[c].fn(d->ctx, devs[i], LIBUSB_HOTPLUG_EVENT_DEVICE_ARRIVED, data);
				}

			}
		}
		
		
		for (i = 0; i < old_count; i++) {
			struct libusb_device_descriptor desc;
			r = libusb_get_device_descriptor(old_devs[i], &desc);
			if (r < 0) // if somehow a device like this ended up in old_dev, delete it
			{
				delete_device(old_devs[i], old_devs, &old_count);
				continue;
			}
			if (desc.idVendor == 0 || desc.idProduct == 0)
			{
				delete_device(old_devs[i], old_devs, &old_count);
				continue;
			}
			if (!find_device(old_devs[i], devs, cnt)) {
				int c;
				for (c = 0; c < hm_state.cb_count; c++) {

					if (hm_state.callbacks[c].fn && (hm_state.callbacks[c].dev_class == (int)desc.bDeviceClass || hm_state.callbacks[c].dev_class == -1)
						&& (hm_state.callbacks[c].vendor_id == (int)desc.idVendor || hm_state.callbacks[c].vendor_id == -1)
						&& (hm_state.callbacks[c].product_id == (int)desc.idProduct || hm_state.callbacks[c].product_id == -1))
						if (hm_state.callbacks[c].events | LIBUSB_HOTPLUG_EVENT_DEVICE_LEFT)
							hm_state.callbacks[c].fn(d->ctx, old_devs[i], LIBUSB_HOTPLUG_EVENT_DEVICE_LEFT, data);
				}
				//remove device from the list
				delete_device(old_devs[i], old_devs, &old_count);
			}
		}


		libusb_free_device_list(devs, 1); //free the list, do not unref devices becuase we unref them when they leave.

	}//END LOOP HERE

}

int hm_start(libusb_context* ctx) {
	if (hm_state.started)
		return -1;
	hm_state.started = 1;
	hm_state.hm_data = malloc(sizeof(hm_data));
	hm_state.hm_data->ctx = ctx;
	hm_state.hm_data->stop = 0;
	thrd_create(&(hm_state.monitor), hm_monitor, hm_state.hm_data);
	return 0;
};

int hm_stop() {
	if (!hm_state.started)
		return -1;
	hm_state.started = 0;
	hm_state.hm_data->stop = 1;
	int res;
	thrd_join(hm_state.monitor, &res);
	return 0;
};

int hm_register_callback(libusb_hotplug_event events, int vendor_id, int product_id, int dev_class, libusb_hotplug_callback_fn cb, void* userData) {
	if (hm_state.cb_count >= MAX_CALLBACKS)
		return -1;
	hm_state.callbacks[hm_state.cb_count].events = events;
	hm_state.callbacks[hm_state.cb_count].dev_class = dev_class;
	hm_state.callbacks[hm_state.cb_count].product_id = product_id;
	hm_state.callbacks[hm_state.cb_count].vendor_id = vendor_id;
	hm_state.callbacks[hm_state.cb_count].fn = cb;
	hm_state.callbacks[hm_state.cb_count].userData = userData;
	hm_state.cb_count++;
	return 0;
}
int hm_remove_callback(libusb_hotplug_callback_fn cb) {
	size_t i;
	for (i = 0; i < MAX_CALLBACKS; i++)
		if (hm_state.callbacks[i].fn == cb) {
			hm_state.callbacks[hm_state.cb_count].events = 0;
			hm_state.callbacks[hm_state.cb_count].dev_class = 0;
			hm_state.callbacks[hm_state.cb_count].product_id = 0;
			hm_state.callbacks[hm_state.cb_count].vendor_id = 0;
			hm_state.callbacks[hm_state.cb_count].fn = 0;
			hm_state.callbacks[hm_state.cb_count].userData = 0;
			hm_state.cb_count++;
			return 0;
		}
	return -1;
};